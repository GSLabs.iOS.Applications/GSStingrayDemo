//
//  ViewController.m
//  GSStingrayDemo
//
//  Created by Dan on 12.11.2021.
//

#import "ViewController.h"

@interface ViewController () <StgBrowserDelegate>

@property StgBrowser *browser;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.browser = [StgBrowser browserWithType:@"_stingray-remote._tcp"];
    [self.browser.delegates addObject:self];
    [self.browser start];
}

- (void)stgBrowser:(StgBrowser *)sender endpointFound:(StgEndpoint *)endpoint {
    [sender stop];
    
    StgClient *client = [StgClient clientWithEndpoint:endpoint userAgent:@"stingray"];
    [self powerSet:client];
//    [self channelsGet:client];
//    [self channelSet:client];
}

- (void)powerSet:(StgClient *)client {
    [client powerSetAsync:YES completion:^(NSError *error) {
        if (error == nil) {
            NSLog(@"Powered on");
        } else {
            NSLog(@"Error - %@", error.localizedDescription);
        }
    }];
}

- (void)channelsGet:(StgClient *)client {
    [client channelsGetAsync:^(NSArray<StgChannel *> *channels, NSError *error) {
        if (channels == nil) {
            NSLog(@"Error - %@", error.localizedDescription);
        } else {
            NSLog(@"%lu channels available", channels.count);
        }
    }];
}

- (void)channelSet:(StgClient *)client {
    StgChannel *channel = StgChannel.new;
    channel.channelListId = @"TV";
    channel.channelNumber = 2;
    
    [client channelSetAsync:channel completion:^(NSError *error) {
        if (error == nil) {
            NSLog(@"Channel changed to - %li", channel.channelNumber);
        } else {
            NSLog(@"Error - %@", error.localizedDescription);
        }
    }];
}

@end
