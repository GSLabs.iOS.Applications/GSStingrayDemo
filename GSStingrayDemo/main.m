//
//  main.m
//  GSStingrayDemo
//
//  Created by Dan on 12.11.2021.
//

#import "StgMain.h"
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass(AppDelegate.class));
    }
}
