//
//  SceneDelegate.h
//  GSStingrayDemo
//
//  Created by Dan on 12.11.2021.
//

#import "StgMain.h"

@interface SceneDelegate : UIResponder <UIWindowSceneDelegate>

@property (strong, nonatomic) UIWindow * window;

@end

